import re
import sys
import datetime as dt
from pathlib import Path

import yaml

main_dir = Path().absolute()
working_dir = main_dir.joinpath("data")
config_file = Path("personal_info.yaml")
web_config = Path("destinations.yaml")
driver_path = r"C:\ProgramData\chocolatey\lib\chromedriver\tools\chromedriver.exe"

form_url = "https://www.nlforms.nl/doc/BS_Declaratieformulier_Yacht_Reis_en_onkosten"

browser_timeout = 30


def error_validation(user_input, error_message):
    print(f"Error input: '{user_input}'. {error_message}.", file=sys.stderr)
    return False


def validate_selection(user_input, value_type, value_list):
    if value_type == "int_opt":
        if user_input in value_list:
            return int(user_input)
        else:
            error_message = f"Input should be {' or '.join(value_list)}"
    elif value_type == "yes_no_opt":
        if user_input[0].lower() in value_list:
            return user_input[0].lower()
        else:
            error_message = "Must be 'yes' or 'no'"
    return error_validation(user_input, error_message)


def validate_date(user_input, value_type):
    if value_type == "birthday":
        try:
            dt.datetime.strptime(user_input, "%d-%m-%Y")
            year_diff = int(
                (dt.datetime.now() - dt.datetime.strptime(user_input, "%d-%m-%Y"))
                / dt.timedelta(days=365)
            )
            if (year_diff < 17) or (year_diff > 70):
                error_message = f"Input date outputs incredible age: {(year_diff)}"
                return error_validation(user_input, error_message)
            else:
                return user_input
        except ValueError:
            error_message = "Input date format 'dd-mm-yyyy'"
            return error_validation(user_input, error_message)


def validate_integer(user_input, value_type):
    if value_type == "integer":
        try:
            return int(user_input)
        except ValueError:
            error_message = "Must be an integer"
        return error_validation(user_input, error_message)
    elif value_type == "int_list":
        try:
            return [int(day) for day in user_input.split(",")]
        except ValueError:
            error_message = "Days must be integers separated by a comma e.g.: 1,2,3"
        return error_validation(user_input, error_message)


def validate_string(user_input, value_type):
    if value_type == "string":
        if re.search("[a-zA-Z]", user_input):
            return user_input
        else:
            error_message = "Must be a string (i.e. contain any alpha character)"
    elif value_type == "e-mail":
        try:
            return re.match(r"^[^\@]+\@[^\@]+\.[^\@]+$", user_input).group(0).lower()
        except ValueError:
            error_message = "No e-mail format (i.e. name@example.com"
    elif value_type == "postcode":
        user_input = "".join(user_input.upper().split())
        postal_code = re.compile("^[0-9]{4}[A-Z]{2}$")
        if postal_code.match(user_input) is not None:
            return user_input
        else:
            error_message = "Postal code format (e.g.) 1234AB"
    return error_validation(user_input, error_message)


def validate_input(prompt, value_type, value_list=None):
    validated_input = False
    while validated_input is False:
        user_input = input(prompt + ":\n")
        if value_type == "optional":
            if user_input.strip() == "":
                validated_input = ""
                break
            else:
                validated_input = validate_string(user_input, "string")
        elif user_input.strip() == "":
            error_validation(user_input, "Input is empty")
        else:
            if value_type in ["string", "e-mail", "postcode"]:
                validated_input = validate_string(user_input, value_type)
            elif value_type in ["integer", "int_list"]:
                validated_input = validate_integer(user_input, value_type)
            elif value_type in ["birthday"]:
                validated_input = validate_date(user_input, value_type)
            elif value_type in ["int_opt", "yes_no_opt"]:
                validated_input = validate_selection(user_input, value_type, value_list)
    return validated_input


def assignment_yaml(yaml_file_name):
    assignment_dict = {}
    while True:
        details_dict = {}
        org_name = validate_input("Organisation name", "string")
        details_dict["days"] = validate_input(
            "(1=Monday, 2= Tuesday, etc.)\nDay numbers separated by commas", "int_list"
        )
        details_dict["from"] = validate_input("From postcode", "postcode")
        details_dict["to"] = validate_input("To Postcode", "postcode")
        details_dict["reason"] = validate_input("Reason", "string")
        details_dict["retour"] = True
        next_input = validate_input(
            "Add new organisation (y/n)", "yes_no_opt", ["y", "n"]
        )
        assignment_dict[org_name] = details_dict
        if next_input[0].lower() == "n":
            break

    with open(yaml_file_name, "w") as output:
        yaml.safe_dump(assignment_dict, output)


def capitalize_name(names):
    return " ".join([name.capitalize() for name in names.split()])


def employee_yaml(yaml_file_name):
    employee_info_dict = {}

    personal_dict = {
        "first_name": capitalize_name(validate_input("First name", "string")),
        "middle_name": validate_input("Middle name", "optional"),
        "last_name": capitalize_name(validate_input("Last name", "string")),
        "email": validate_input("E-mail", "e-mail"),
        "birthday": validate_input("Birthday (dd-mm-yyyy)", "birthday"),
        "employee": ["geg_employeesoortopt2", "geg_employeesoortopt3"][
            validate_input("Employee (1=ZZP, 2=Professional)", "int_opt", ["1", "2"])
            - 1
        ],
    }

    expenses_dict = {
        "consultant": validate_input("E-mail consultant", "e-mail"),
        "declaration": [
            "dr_keuze_kostensoortopt4-label",
            "dr_keuze_kostensoortopt7-label",
        ][validate_input("Declaration type (1=Auto, 2=Ov)", "int_opt", ["1", "2"]) - 1],
    }

    employee_info_dict["personal_info"] = personal_dict
    employee_info_dict["expenses"] = expenses_dict

    with open(yaml_file_name, "w") as output:
        yaml.safe_dump(employee_info_dict, output, default_flow_style=False)


def create_config():
    employee_yaml("personal_info.yaml")
    assignment_yaml("destinations.yaml")
