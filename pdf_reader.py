from PyPDF3 import PdfFileReader
import re

import config as cfg


def read_pdf(file_path):
    """
    Opens and reads pdf file and returns the pdf text
    :param file_path: name of file to open
    :return: raw pdf text
    """
    return PdfFileReader(open(file_path, "rb"))


def get_info(pdf_file, info=None):
    """
    Gets total price out of raw pdf text
    :param pdf_file: text of ov-overview pdf file
    :param info: describes which information to extract
    :return: total ov-cost price
    """
    if info == "price":
        price_regex = re.compile(r'(\d+,\d{2}) inclusief \d% btw')
        return float(price_regex.findall(pdf_file.lower())[0].replace(',', '.'))


def main(file_name):
    file_path = cfg.working_dir.joinpath(file_name)

    pdf_file = read_pdf(file_path)
    pdf_text = pdf_file.getPage(0).extractText()
    return get_info(pdf_text, info="price")


if __name__ == '__main__':
    main()
