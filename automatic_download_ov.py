from selenium import webdriver
import selenium as sel
from selenium.common.exceptions import NoSuchElementException, InvalidElementStateException, ElementNotVisibleException, ElementNotInteractableException, WebDriverException
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support.ui import Select
import selenium.webdriver.support.expected_conditions as ec
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By

import requests

import datetime
import time
import os
from web_drivers import return_webdriver_location

def create_browser(webdriver_to_use, overwrite_default = False):
    webdriver_to_use = webdriver_to_use.capitalize()
    driver_name, location = return_webdriver_location(webdriver_to_use, overwrite_default)
    driver = {"Firefox": webdriver.Firefox, "Chrome": webdriver.Chrome}
    if driver_name != webdriver_to_use:
        print("Not using selected driver!")
    return driver[driver_name](executable_path = location)
    
def set_item_to(browser, item_name, text, wait = False):
    if wait:
        element = wait_for_element(browser, item_name)
    # make sure the item is empty
    for x in range(0,100):
        try:
            element = browser.find_element_by_name(item_name)
            assert element.get_attribute('type') == 'text'
            if element.get_attribute('value') != '':
                element.clear()
        except (ElementNotInteractableException, ElementNotVisibleException, InvalidElementStateException):
            browser.implicitly_wait(1)
            continue
        except (AssertionError):
            print("Element is not text")
        else:
            break
    else:
        return "Failed"
            
    for x in range(0,30):
        try:
            element.send_keys(text + Keys.TAB)
        except (ElementNotInteractableException, ElementNotVisibleException, InvalidElementStateException):
            browser.implicitly_wait(1)
            continue
        except Exception as e:
            print("Error:", e)
            browser.implicitly_wait(1)
            continue
        # except ElementNotVisibleException:
            # browser.implicitly_wait(1)
            # continue
        # except InvalidElementStateException:
            # browser.implicitlyWait(1)
        else:
            break
    # else:
        # element.send_keys(text, Keys.TAB)
    return element
            #ec.invisibility_of_element_located

def click_radio_button(browser, item_name, option):
    radio_items = browser.find_elements_by_name(item_name)
    for item in radio_items:
        if item.get_attribute('type') != 'radio':
            continue
        if item.get_attribute("value") == option:
            for x in range(0,50):
                try:
                    item.click()
                    break
                except WebDriverException:
                    browser.implicitly_wait(5)
                    continue
            break
    else:
        print("Value", option, "not found, options include:")
        {print("'" + x.get_attribute("value") + "'") for x in radio_items}
    return radio_items
    
def click_checkbox_button(browser, item_name, option):
    options = browser.find_elements_by_name(item_name)
    for item in options:
        if item.get_attribute('type') != 'checkbox':
            continue
        if item.get_attribute("value") == option:
            for x in range(0,50):
                try:
                    item.click()
                    break
                except WebDriverException:
                    browser.implicitly_wait(5)
                    continue
            break
    else:
        print("Value", option, "not found, options include:")
        {print("'" + x.get_attribute("value") + "'") for x in options}
    return options
    
def click_button(browser, item_name):
    button = browser.find_element_by_name(item_name)
    assert button.get_attribute('type') == 'button', 'Element is not a button'
    for i in range(0,60):
        try:
            button.click()
            break
        except:
            browser.implicitly_wait(5)
            continue

def click_item_list(browser, item_name, option):
    element = browser.find_element_by_name(item_name)
    option_list = Select(element)
    option = option.lower()
    for each_option in option_list.options:
        if option in each_option.get_attribute('value').lower():
            option_list.select_by_value(each_option.get_attribute('value'))
            return element, each_option, option_list
            # click_item_list(browser, 'dr_inleiding_week_2018', 'Week 1')
    else:
        print(f"Element {option} not found in list.")
    
    
def wait_for_element(browser, item_name):
    return WebDriverWait(browser, 10).until(lambda x: x.find_element_by_name(item_name))

def upload_file(browser, file_name, number):
    file_name = os.path.abspath(file_name)
    if not os.path.isfile(file_name):
        print("File not found", file_name)
        return
    file_uploaders = browser.find_elements_by_css_selector('input[type="file"]')
    file_uploaders[number].send_keys(file_name)

def upload_ov_file(browser, file_name):
    # OV1 ID = 0, OV2 = 1, car costs = e.g 4 etc.
    upload_file(browser, file_name, 0)
    
def set_date(browser, year, week):
    year = str(year)
    click_radio_button(browser, "dr_inl_opgave_jaar", year) # voor welk jaar
    click_item_list(browser, "dr_inleiding_week_" + year, "Week " + week + "\t") # voor welke week

def get_list_of_cards(browser):
    list_of_cards = []
    for x in range(0,10):
        if len(list_of_cards) == 0:
            list_of_cards = browser.find_elements_by_class_name("cs-card-title")
            browser.implicitly_wait(5)
        else:
            break
    return list_of_cards

def choose_card(browser, name_or_id):
    list_of_cards = get_list_of_cards(browser)
    
    element_by_card_name = dict()
    element_by_card_id = dict()
    for i, card in enumerate(list_of_cards):
        card_name, card_id = card.text.split('\n')
        element_by_card_name[card_name] = list_of_cards[i]
        element_by_card_id[card_id] = list_of_cards[i]
    
    card_selection = element_by_card_name.get(name_or_id, element_by_card_id.get(name_or_id)) # try first dict, then second
    card_selection.click()
    
    return card_selection

def get_value(browser, name):
    return browser.find_element_by_name(name).get_attribute('value')

def download_summary(browser, year, week, begin_date, end_date, docfmt, selected_transactions = None):
    file_url = "https://www.ov-chipkaart.nl/web/document/"
    cookies = {x['name']:x['value'] for x in browser.get_cookies()}
    
    # required for the POST
    swfrmsig = get_value(browser, "swfrmsig")
    if not selected_transactions:
        selected_transactions = get_value(browser, "selectedTransactions")
    mid = get_value(browser, "mediumid")
    lang = "nl-NL"
    docfmt = docfmt # CSV or PDF
    file_name = year + " Week " + week + "." + docfmt.lower()
    # building POST body
    """r=requests.post(url = "https://www.ov-chipkaart.nl/web/document/?mediumid=35
    28043420811389&begindate=21-01-2019&enddate=25-01-2019&languagecode=nl-NL&select
    edTransactions=%7B%22578%22%3Atrue%2C%22579%22%3Atrue%2C%22580%22%3Atrue%2C%2258
    1%22%3Atrue%2C%22582%22%3Atrue%2C%22583%22%3Atrue%2C%22584%22%3Atrue%2C%22585%22
    %3Atrue%2C%22586%22%3Atrue%2C%22587%22%3Atrue%7D&type=&documentFormat=CSV&swfrms
    ig="+str(x), cookies = cookies) # x=swfrmsig"""
    body= f"mediumid={mid}&begindate={begin_date}&enddate={end_date}&languagecode={lang}&selectedTransactions={selected_transactions}&type=&documentFormat={docfmt}&swfrmsig={swfrmsig}"
    file_url += "?" + body
    # downloading file
    file_download_request = requests.post(file_url, cookies = cookies)
    with open(file_name, 'wb') as f:
        f.write(file_download_request.content)
    
    if file_download_request.ok:
        print(f"Download completed of {file_name} on {file_url}\n")
        
    else:
        print(f"Download of {file_name} not completed: {file_download_request.reason}\n")
        
    return file_name

def sum_csv_data(csv_file):
    with open(csv_file, 'r') as data:
        sum = 0
        header = data.readline()
        line = header.strip().split(';')
        for i, item in enumerate(line):
            if item[1:-1].lower() == "bedrag":
                costs_column = i
                break
        else:
            print("Could not find column with costs.")
            costs_column = 5 # default
        
        for line in data:
            try:
                if "Saldo opgeladen" in line:
                    continue
                sum += float(line.strip().split(';')[costs_column][1:-1].replace(',','.'))
            except ValueError:
                continue
    sum = round(sum, 2)
    print("Total sum:", str(sum), "\n")
                
    return sum
    

def download_ov(browser, year, week, card = '2', start_hour = None, end_hour = None, work_days = None):
    year = str(year)
    week = str(week)
    browser.get("https://www.ov-chipkaart.nl/mijn-ov-chip/mijn-ov-reishistorie.htm") #("https://www.ov-chipkaart.nl/mijn-ov-chip.htm")
    # browser.get("https://www.ov-chipkaart.nl/my-ov-chip/my-travel-history.htm") # in case of English
    # browser.manage().timeouts().implicitlyWait() # wait until fully loaded (re)load of URL
    for x in range(0,10):
        if "https://login.ov-chipkaart.nl/auth" == browser.current_url[:34]:
            break
        browser.implicitly_wait(5)
    for x in range(0, 60):
        if "https://login.ov-chipkaart.nl/auth" == browser.current_url[:34]:
            print("Please log in...")
            browser.implicitly_wait(5)
        else:
            break
    
    list_of_cards = []
    for x in range(0,10):
        if len(list_of_cards) == 0:
            list_of_cards = browser.find_elements_by_class_name("cs-card-title")
            browser.implicitly_wait(5)
        else:
            break
    
    choose_card(browser, card)
    browser.implicitly_wait(5)
    
    year_week = year + "-" + week
    begin_date = datetime.datetime.strptime(year_week + '-1', "%G-%V-%u").strftime("%d-%m-%Y") # Monday
    end_date = datetime.datetime.strptime(year_week + '-5', "%G-%V-%u").strftime("%d-%m-%Y") # Friday
    first_part_url, second_part_url = browser.current_url.split("?", 1)
    url = first_part_url + "?" + "begindate=" + begin_date + "&enddate=" + end_date + "&" + second_part_url
    browser.get(url)
    
    if work_days == []:
        print("No workdays selected")
        return
    
    transactions = None
    total = None
    wait = WebDriverWait(browser, 30)
    element = wait.until(ec.presence_of_element_located((By.CLASS_NAME, "toggleAllSelection")))
    if all((start_hour, end_hour, work_days)): # valid selection of hours/days, else assume all should be selected
        total, transactions = filter_website(browser, start_hour, end_hour, work_days)
    
    for item in browser.find_elements_by_class_name("tlsBtnSmall"):
        if (item.get_attribute('value') == "Maak declaratieoverzicht" or item.get_attribute('value') == "Create expenses overview"):
            item.send_keys(Keys.ENTER)
            break
    
    # waiting for page loading
    for x in range(0,10):
        if ("https://www.ov-chipkaart.nl/mijn-ov-chip/mijn-ov-reishistorie/ov-reishistorie-declaratie.htm" == browser.current_url[:92] or 
        "https://www.ov-chipkaart.nl/my-ov-chip/my-travel-history/travel-history-declaration.htm" == browser.current_url[:87]):
            break
        browser.implicitly_wait(5)
    else:
        print(f"No travels found for {year}, week {week} or error occurred, see webbrowser...")
        return
    
    csv = download_summary(browser, year, week, begin_date, end_date, "CSV", transactions)
    pdf = download_summary(browser, year, week, begin_date, end_date, "PDF", transactions)
    return csv, pdf, total
    
        
    # browser.find_element_by_class_name("toggleAllSelection").click()
        
    # for x in browser.find_elements_by_class_name("cs-card-alias"):
        # pass
    # return list_of_cards

def select_this_page_transactions(browser, start, end, work_days):
    assert isinstance(start, datetime.time), "start time should be of type datetime.time"
    assert isinstance(end, datetime.time), "start time should be of type datetime.time"
    
    wait = WebDriverWait(browser, 30)
    element = wait.until(ec.presence_of_element_located((By.CLASS_NAME, "known-transaction"))) # element is not used, just for waiting
    this_page_transactions = browser.find_elements_by_class_name("known-transaction")
    split_transactions = [item.text.split('\n') for item in this_page_transactions]
    list_of_checkins = [item[-1].split(' ')[-1].replace(',','.') for item in split_transactions]
    times = [item[2].split(' ')[0] for item in split_transactions]
    dates = [datetime.datetime.strptime(item[0], "%d-%m-%Y") for item in split_transactions]
    page_total = 0
    selected_page_transactions = {} #{"578":True,"579":True,"580":True,"581":True,"582":True,"583":True,"584":True,"585":True,"586":True,"587":True}
    # {print(x) for x in split_transactions if "Check-uit" in x[1]}
    for i, item in enumerate(this_page_transactions):
        wait = WebDriverWait(browser, 30)
        element = wait.until(ec.presence_of_element_located((By.CLASS_NAME, "toggleAllSelection")))
        transaction_id = this_page_transactions[i].find_element_by_class_name("transactionOverviewCheckbox").get_attribute("transactionid")
        
        try:
            cost = float(list_of_checkins[i]) # assume that everything that can be converted to floats is an amount
        except ValueError:
            selected_page_transactions[transaction_id] = False
            continue
        
        page_total += cost
        hours, minutes = times[i].split(":")
        t = datetime.time(int(hours), int(minutes))
        
        if t < start or t > end:
            selected_page_transactions[transaction_id] = False
            continue
        elif dates[i].weekday() not in work_days:
            selected_page_transactions[transaction_id] = False
            continue
        else:
            selected_page_transactions[transaction_id] = True
        
    return selected_page_transactions, page_total

def dict_replace(string, replacements):
    for key, value in replacements.items():
        string = string.replace(key, value)
    return string
    
def filter_website(browser, start_hour, end_hour, list_of_work_days):
    # filteren op basis van per dag per uur
    # haal bedrag op tussen start- en eindtijd van gewerkte dagen alleen check-out
    # download pdf
    start = datetime.time(start_hour, 0)
    if end_hour > 23:
        end = datetime.time(23, 59)
    else:
        end = datetime.time(end_hour, 0)
    # day_conversion = {1: 'maandag', 2: 'dinsdag', 3: 'woensdag', 4: 'donderdag', 5: 'vrijdag', 6: 'zaterdag', 0: 'zondag'}
    # day_conversionEN = {1: 'Monday', 2: 'Tuesday', 3: 'Wednesday', 4: 'Thursday', 5: 'Friday', 6: 'Saturday', 0: 'Sunday'}
    # list_of_work_days = [day_conversion.get(day, 'nothing') for day in list_of_work_days]
    work_days = set(list_of_work_days)
    # page_numbers = [int(element.get_attribute('value')) for element in browser.find_elements_by_name('pagenumber') if element.get_attribute('label') and element.get_attribute('label')[:2]=='Pa']
    total_amount = 0
    all_transactions = {}
    last_page = False
    for x in range(0, 1000): # max number of possible pages (takes too long to load those either way)
        if last_page:
            break
        # print("Click to page"+str(page+1))
        transactions, page_total = select_this_page_transactions(browser, start, end, work_days)
        all_transactions = {**all_transactions, **transactions}
        total_amount += page_total
        for button in browser.find_elements_by_name('pagenumber'):
            if button.get_attribute('label') == "Volgende pagina" or button.get_attribute('label') == "Next Page":
                button.click()
                break
        else:
            last_page = True
    transactions, page_total = select_this_page_transactions(browser, start, end, work_days)
    all_transactions = {**all_transactions, **transactions}
    total_amount += page_total
    
    # all_transactions = {key:value for key, value in all_transactions.items() if int(key) >= 1000} # should always be the case
    # string_transactions = str(all_transactions).replace('True', 'true').replace('False', 'false')
    string_transactions = dict_replace(str(all_transactions), {'True':'true', 'False':'false', "'":'"', ' ':''})
    return total_amount, string_transactions

def ov_kosten(browser, year, week, cost, file):
    browser.get("https://www.nlforms.nl/doc/BS_Declaratieformulier_Yacht_Reis_en_onkosten")

    set_item_to(browser, "geg_voornaam", "Richard", wait = True) # voornaam
    set_item_to(browser, "geg_achternaam", "Groen") # achternaam
    set_item_to(browser, "geg_e_mailadres", "richard.groen.a@yacht.nl") # yacht email adres
    set_item_to(browser, "geg_geboortedatum", "27-04-1990") # geboortedatum
    click_radio_button(browser, "geg_employeesoort", "Professional") # Professional of ZZP'er
    click_button(browser, "btnNext") # volgende pagina

    set_item_to(browser, "dr_inleiding_email_LG", "francesco.doeve@yacht.nl") # consultant email adres
    set_date(browser, str(year), str(week)) # voor welke week is de declaratie
    click_checkbox_button(browser, "dr_keuze_kostensoort", "OV-kosten") # type kosten (OV-kosten etc.)
    click_button(browser, "btnNext") # volgende pagina

    set_item_to(browser, "dr_OV_bedrag_1", str(cost)) # totaal aantal kosten
    upload_ov_file(browser, file)
    click_radio_button(browser, "dr_ov_woonwerkvraag", "Nee")
    click_button(browser, "btnNext")    
    click_button(browser, "btnNext")
    # click_button(browser, "btnSubmit")
    # done uploading
    # click_button(browser, "btnNext")
    
# main()
# def main():
# browser = create_browser()
# year = 2019
# week = 4
# card = "2"
# download_ov(browser, year, week, card) # temporary

def test_transactions(browser, start, end, list_of_work_days):
    total, found_transactions = filter_website(browser, start, end, list_of_work_days))
    for item in browser.find_elements_by_class_name("tlsBtnSmall"):
        if (item.get_attribute('value') == "Maak declaratieoverzicht" or item.get_attribute('value') == "Create expenses overview"):
            item.send_keys(Keys.ENTER)
            break
    
    # waiting for page loading
    for x in range(0,10):
        if ("https://www.ov-chipkaart.nl/mijn-ov-chip/mijn-ov-reishistorie/ov-reishistorie-declaratie.htm" == browser.current_url[:92] or 
        "https://www.ov-chipkaart.nl/my-ov-chip/my-travel-history/travel-history-declaration.htm" == browser.current_url[:87]):
            break
        browser.implicitly_wait(5)
    else:
        print(f"No travels found for {year}, week {week} or error occurred, see webbrowser...")
        return
    found_transactions2 = get_value(browser, "selectedTransactions")
    return found_transactions, found_transactions2

def main(year, week, card, start_hour = None, end_hour = None, workdays = None):
    csv, pdf, costs = download_ov(browser, year, week, card, start_hour, end_hour, workdays)
    if not costs:
        costs = sum_csv_data(csv)
    ov_kosten(browser, year, week, costs, pdf)

if __name__ == '__main__':
    webdriver_to_use = "Chrome"
    browser = create_browser(webdriver_to_use)
# browser.close()
# browser.quit()