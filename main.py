import time
import datetime

import yaml
import pyautogui
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import Select
from selenium.webdriver import ChromeOptions, Chrome
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

import config as cfg
import pdf_reader as pr
from automatic_download_ov import create_browser


# Install chocolatey ([Command Prompt] or PowerShell) - https://chocolatey.org/install
# Install chrome webdriver - "choco install chromedriver"
# Modify config.driver_path for your chromedriver location or add to PATH - https://bit.ly/2COZBe6


def current_week():
    """
    Retrieves the current iso-week of the year.
    :return: current iso-week
    """
    return datetime.date.today().isocalendar()[1]


def current_year():
    """
    Retrieves the current iso-year.
    :return: current iso-year
    """
    return datetime.date.today().year


def get_week_input():
    """
    Allows for user input to define which week to use.
    Default: current week.
    :return: week
    """
    week = current_week()
    input_week = input(f"What week? (default: {week})")
    if input_week:
        week = int(input_week)
    return week


def get_year_input():
    """
    Allows for user input to define which year to use.
    Default: current year.
    :return: year
    """
    year = current_year()
    input_year = input(f"What year? (default: {year})")
    if input_year:
        year = int(input_year)
    return year


def get_day_of_week(year, week, day):
    """
    Obtains the date of the day of the week in given year.
    :param year: year of date
    :param week: week of date
    :param day: day of date
    :return: date of weekday
    """
    weekday_date = datetime.datetime.strptime(f'{year} {week} {day}', '%G %V %u')
    weekday_date = weekday_date.strftime("%d-%m-%Y")
    return weekday_date


def get_config(configuration):
    """
    Checks if configuration files exist.
    If configuration files do not exist, goes to the setup to create the configuration files.
    :param configuration: path to the configuration file
    :return: configuration information
    """
    if not configuration.is_file():
        print("No configuration file found, starting setup...")
        print()
        cfg.create_config()
    with open(configuration, "rb") as f:
        config = yaml.safe_load(f)
    return config


def init_selenium():
    """
    Initialises a Selenium browser object.
    :return: browser object
    """
    opts = ChromeOptions()
    opts.add_experimental_option("detach", True)
    browser = Chrome(executable_path=cfg.driver_path, options=opts)
    browser.get(cfg.form_url)
    return browser

def init_browser():
    # NOTE: this function will figure out where the driver is or download it if required
    browser = create_browser('Chrome') # will try to use Chrome or download it, if not possible, will try using Firefox
    return browser

def next_page(browser):
    """
    Goes to the next page of the browser.
    :param browser: browser object
    :return: browser object
    """
    next_button = browser.find_element_by_id("tdNext")
    next_button.click()


def first_page(browser, config):
    """
    Fills in all fields on the first page of the declaration form.
    :param browser: browser object
    :param config: configuration dictionary
    :return: browser object
    """
    element = WebDriverWait(browser, 30).until(
        EC.visibility_of_element_located((By.ID, "geg_voornaam"))
    )

    personal_info = config["personal_info"]
    element.send_keys(personal_info["first_name"])

    middle_name = browser.find_element_by_id("geg_tussenvoegsel")
    middle_name.send_keys(personal_info["middle_name"])

    last_name = browser.find_element_by_id("geg_achternaam")
    last_name.send_keys(personal_info["last_name"])

    email = browser.find_element_by_id("geg_e_mailadres")
    email.send_keys(personal_info["email"])

    birth_date = browser.find_element_by_id("geg_geboortedatum")
    birth_date.send_keys(personal_info["birthday"])

    # geg_employeesoortopt2 - ZZP
    # geg_employeesoortopt3 - Professional
    employee = browser.find_element_by_id(personal_info["employee"])
    employee.click()


def second_page(browser, config, declaration_year, declaration_week):
    """
    Fills in all fields on the second page of the declaration form.
    All values are derived from the configuration dictionary.
    :param browser: browser object
    :param config: configuration dictionary
    :param declaration_year: year to be declared
    :param declaration_week: week to be declared
    :return: browser object
    """
    expenses = config["expenses"]

    element = WebDriverWait(browser, 30).until(
        EC.visibility_of_element_located((By.ID, "dr_inleiding_email_LG"))
    )

    # E-mail consultant
    # dr_inleiding_email_LG
    element.send_keys(expenses["consultant"])

    # Jaartal
    # dr_inl_opgave_jaaropt1-label - vorig jaar?
    # dr_inl_opgave_jaaropt2-label - huidig jaar?
    # year = browser.find_element_by_id(expenses["year"])
    year = browser.find_element_by_xpath(f"//input[@value={declaration_year}]")
    year.click()

    # Week
    # dr_inleiding_week_2019
    week = Select(browser.find_element_by_id("dr_inleiding_week_2019"))
    week.select_by_index(declaration_week)

    # Soort declaratie
    # dr_keuze_kostensoortopt1-label - Zakelijke kilometers
    # dr_keuze_kostensoortopt2-label - Maaltijd
    # dr_keuze_kostensoortopt3-label - Parkeerkosten
    # dr_keuze_kostensoortopt4-label - Woon-werkverkeer
    # dr_keuze_kostensoortopt5-label - Overige kosten (VOG)
    # dr_keuze_kostensoortopt6-label - Verblijfskosten
    # dr_keuze_kostensoortopt7-label - OV-kosten
    declaration = browser.find_element_by_id(expenses["declaration"])
    declaration.click()


def commute(browser, declaration_week, declaration_year):
    """
    Fills in all the required fields for declaring a commute.
    All values are derived from the configuration dictionary.
    :param browser: browser object
    :param declaration_week: week of declaration
    :param declaration_year: year of declaration
    :return: browser object
    """
    config = get_config(cfg.web_config)
    num_day = 1

    for location in config:
        days = config[location]["days"]
        location_from = config[location]["from"]
        location_to = config[location]["to"]
        reason = config[location]["reason"] + " " + location

        for day in days:
            date_field = WebDriverWait(browser, cfg.browser_timeout).until(
                EC.visibility_of_element_located((By.ID, f"dr_km_datum1{num_day}"))
            )
            date_of_day = get_day_of_week(year=declaration_year, week=declaration_week, day=day)
            # Datum veld
            # dr_km_datum11
            date_field.send_keys(date_of_day)

            # Reisdoel
            # dr_km_reisdoel_11
            goal = browser.find_element_by_id(f"dr_km_reisdoel_1{num_day}")
            goal.send_keys(reason)

            # Adres vertrek
            # dr_km_adres_vertrek11
            address_depart = browser.find_element_by_id(f"dr_km_adres_vertrek1{num_day}")
            address_depart.send_keys(location_from)

            # Adres aankomst
            # dr_km_adres_aankomst11
            address_arrive = browser.find_element_by_id(f"dr_km_adres_aankomst1{num_day}")
            address_arrive.send_keys(location_to)

            # Retour
            # dr_km_retour_11
            retour = browser.find_element_by_id(f"dr_km_retour_1{num_day}")
            num_day += 1
            retour.click()


def public_transport(browser, expense_amount, file_name):
    """
    Calls function to read PDF file and retrieves declaration amount.
    Fills in required information.
    :param browser: browser object
    :param expense_amount: amount to be declared
    :param file_name: name of the pdf to read
    :return: browser object
    """
    pt_amount = WebDriverWait(browser, cfg.browser_timeout).until(
        EC.visibility_of_element_located((By.ID, "dr_OV_bedrag_1"))
    )
    pt_amount.send_keys(str(expense_amount))
    browser.find_element_by_id("dr_OV_bijlage_1").click()
    time.sleep(2)
    pyautogui.typewrite(str(cfg.working_dir.joinpath(file_name)))
    time.sleep(3)
    pyautogui.press('enter')


def process_options(browser, config, week, year):
    """
    Decides which declaration function to call,
    based on the type of declaration specified in the configuration settings.
    :param browser: browser object
    :param config: configuration dictionary
    :param week: week of declaration
    :param year: year of declaration
    :return: browser object
    """
    declaration_type = config["expenses"]
    if declaration_type["declaration"] == "dr_keuze_kostensoortopt4-label":
        commute(browser=browser, declaration_week=week, declaration_year=year)
    elif declaration_type["declaration"] == "dr_keuze_kostensoortopt7-label":
        public_transport_file = f"declaratieoverzicht_{year}{week}.pdf"
        declaration_amount = pr.main(public_transport_file)
        public_transport(browser=browser, expense_amount=declaration_amount, file_name=public_transport_file)
    pass


def main():
    week = get_week_input()
    year = get_year_input()

    print()
    print(f"Travel expenses for week {week}, year {year}:")

    browser = init_selenium()
    personal_config = get_config(cfg.config_file)
    first_page(browser, personal_config)
    next_page(browser)
    second_page(browser, personal_config, year, week)
    next_page(browser)
    process_options(browser, personal_config, week, year)
    # overzichtspagina
    next_page(browser)
    # comment pagina
    # next_page(browser)

    print()
    print("Done!")


if __name__ == "__main__":
    main()
