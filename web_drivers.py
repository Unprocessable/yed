from urllib import request

from platform import architecture, system
import os
import glob

from zipfile import ZipFile as zf
import tarfile

def get_bit_version():
    arch = architecture()[0]

    if "32" in arch:
        bit_version = 32
    
    elif "64" in arch:
        bit_version = 64
    
    else:
        print("Could not determine OS bit version automatically, assuming 32 bits.")
        bit_version = 32
   
    return bit_version

def download_file(URL, working_directory = '.'):
    if working_directory == '.':
        filename = os.path.abspath(os.path.split(URL)[1])
    else:
        filename = os.path.join(working_directory, os.path.split(URL)[1])
        
    print("Downloading file from", str(URL))
    with open(filename, 'wb') as download:
        download.write(request.urlopen(URL).read())
        
    print("Done downloading to", filename)
        
    return filename
    
def extract(file_name, in_folder = '.'):
    file_extension = os.path.splitext(file_name)[1]
    
    if file_extension == '.gz':
        print("Mac or Linux not implemented yet.")
        location = untar(file_name, in_folder)
        # probably have to chmod +x too before allowing execution
        # see https://stackoverflow.com/questions/279945/set-permissions-on-a-compressed-file-in-python
    elif file_extension == '.zip':
        location = unzip(file_name, in_folder)
        
    return location
    
def untar(tarfile, directory):
    # NOTE: Not protected against malicious data
    directory = os.path.abspath(directory)
    try:
        tar = tarfile.open(tarfile)
        tar.extractall(directory)
    except Exception as e:
        raise(e)
    finally:
        tar.close()
    return directory
    
def unzip(zipfile, directory):
    directory = os.path.abspath(directory)
    try:
        zipped_file = zf(zipfile)
        zipped_file.extractall(directory)
    except Exception as e:
        raise(e)
    finally:
        zipped_file.close()
    return directory
    
def get_drivers(in_folder):
    # first match (there is probably an easier way)
    return {f for f in listdir(in_folder) if isfile(join(in_folder, f)) and "driver" in join(in_folder, f)}

def download_firefox_driver(in_folder = '.'):
    # check if already exists before calling this
    latest = request.urlopen("https://github.com/mozilla/geckodriver/releases/latest")
    latest_version = os.path.split(latest.geturl())[1]
    bits = get_bit_version()
    os_name = system()
    
    if os_name == "Windows":
        default_url = f"https://github.com/mozilla/geckodriver/releases/download/{latest_version}/geckodriver-{latest_version}-win{bits}.zip"
    elif os_name == "Darwin":
        # Mac OS
        default_url = f"https://github.com/mozilla/geckodriver/releases/download/{latest_version}/geckodriver-{latest_version}-macos.tar.gz"
        
    elif os_name == "Linux":
        default_url = f"https://github.com/mozilla/geckodriver/releases/download/{latest_version}/geckodriver-{latest_version}-linux{bits}.tar.gz"
    
    downloaded_file = download_file(default_url)
    driver_location = extract(downloaded_file)
        
    return driver_location
    
def ask_version():
    version = input('Which version of Chrome are you running (max 3 digit): ')
            
    try:
        chrome_version = int(version)
    except:
        print('Provide a number for Chrome\'s version please.')
        return 0
        
    if len(version) > 3:
        print('Chrome version should be less than 3 digits.')
        return 0
        
    return chrome_version
    
def get_latest_chromedriver_version(chrome_version):
    if chrome_version < 70:
        last_part = "LATEST_RELEASE"
    else:
        last_part = "LATEST_RELEASE_"+str(chrome_version)
        
    storage_location = 'https://chromedriver.storage.googleapis.com/'
    data = request.urlopen(storage_location + last_part).read()
    driver_version = str(data.decode())
    driver_version = driver_version.strip()
    return driver_version
    
def download_chromedriver(in_folder = '.'):
    os_name = system()
    chrome_location = [""]
    
    if os_name == "Windows":
        chrome_location = glob.glob('C:\\Program Files*\\Google\\Chrome\\Application\\??.*')
        
        if len(chrome_location) == 0:
            chrome_location = glob.glob(os.environ['USERPROFILE']+'\\Local Settings\\Application Data\\Google\\Chrome\\Application\\??.*')
        
        if len(chrome_location) == 0:
            chrome_location = glob.glob('D:\\Program Files*\\Google\\Chrome\\Application\\??.*')
        
        
    loc = ver = "not set"
    chrome_version = 0
        
    try:
        loc = os.path.split(chrome_location[0])[-1]
        ver = loc.split('.')[0]
        chrome_version = int(ver)
    except Exception as e:
        print("Error occurred:", e)
        print(loc, ver, chrome_version)
        chrome_version = ask_version()
    
    if chrome_version == 0:
        print("Could not determine Chrome version, exiting...")
        return
        
    driver_version = get_latest_chromedriver_version(chrome_version)
    default_part = f"https://chromedriver.storage.googleapis.com/{driver_version}/"
    if os_name == "Windows":
        os_part = "chromedriver_win32.zip"
    elif os_name == "Darwin":
        os_part = "chromedriver_mac64.zip"
    else:
        os_part = "chromedriver_linux64.zip"
    
    downloaded_file = download_file(default_part + os_part)
    driver_location = extract(downloaded_file)
    
    return driver_location

def check_geckodriver():
    os_name = system()
    if os_name == "Windows":
        filename = "geckodriver.exe"
    else:
        filename = "geckodriver"
    
    if os.path.isfile(filename):
        return os.path.abspath(filename)
    
    return False

def check_chromedriver():
    os_name = system()
    if os_name == "Windows":
        filename = "chromedriver.exe"
    else:
        filename = "chromedriver"
        
    if os.path.isfile(filename):
        return os.path.abspath(filename)
    
    return False
    
def return_webdriver_location(default = 'firefox', overwrite_default = True):
    list_of_drivers = {'firefox': check_geckodriver, 'chrome': check_chromedriver}
    default = default.lower()
    
    if list_of_drivers[default]():
        print("Using selected driver", str(default).capitalize())
        return str(default).capitalize(), list_of_drivers[default]()
    elif overwrite_default:
        print("Default driver for", str(default).capitalize(), "is not available.")
        for item in list_of_drivers:
            if list_of_drivers[item]():
                print("Using", str(item).capitalize())
                return str(item).capitalize(), list_of_drivers[item]()
                    
    for i in range(0, 9):
        try:
            print("Which webdriver should be downloaded?")
            driver = input("Press 1 for Firefox, press 2 for Chrome and then Enter:\n")
            if driver not in ['1', '2']:
                print("Invalid choice. Try again (" + str(i + 1), "of 10 times).")
                continue
            driver = driver.strip()
            possible_drivers = {'1': ("Firefox", download_firefox_driver), 
                                '2': ("Chrome", download_chromedriver)}
            name, download_driver = possible_drivers[driver] # assigns either check_geckodriver or check_chromedriver function to check_driver
        except Exception as e:
            print("Error", e, "occurred. Try again (" + str(i + 1), "of 10 times).")
            continue
        else:
            break
    
    download_driver() # function assigned from dictionary above (don't look for def check_driver)
    
    return name, list_of_drivers[name.lower()]()